import Vue from 'vue'
import Router from 'vue-router'
import abc from '@/components/abc'
import admin from '../components/child1'
import child2 from '../components/child2'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'abc',
      component: abc,
      children:[
        {
          path: '/admin',
      name: 'admin',
      component: admin,
        },
        {
          path: '/child2',
          name: 'child2',
          component: child2,
        }
      ]
      
    }
  ]
})
